package ru.ikaar.android.projects.smsgatewayserviceclient;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class WebSocketClass {
    public Socket socket;
    public Context context;
    public String connnectionId;
    WebSocketClass(String url, Context ctx){
        try{
            this.socket = IO.socket(url);
            context = ctx;
            Handler handle = new Handler(){
                @Override
                public void handleMessage(Message msg){
                    Toast.makeText(context, "Соединение установлено", Toast.LENGTH_SHORT).show();
                }
            };
            connect(handle);
        }catch(URISyntaxException e){
            e.printStackTrace();
        }
    }
    public void connect(Handler handler){
        createEvent(Socket.EVENT_CONNECT, handler);
        socket.connect();
    }
    public void createEvent(String eventName, final Handler handler){
        this.socket.on(eventName, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Message message = new Message();
                if(args.length >= 1)
                    message.obj = args[0].toString();
                handler.sendMessage(message);
            }
        });
    }
    public void disconnect(){
        socket.disconnect();
    }
}
