package ru.ikaar.android.projects.smsgatewayserviceclient;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    public Intent serviceIntent;
    public AppCompatActivity activity;
    //View elements
    EditText edtStatus;
    Button btnServiceStart;
    Button btnServiceStop;

    //Handlers
    Handler hMessage;
    Handler hService;
    Handler hSocket;
    Handler hSendMessage;
    PendingIntent sentPI;
    PendingIntent deliveredPI;
    //  *** Other variables ***

    MessageClass m;

    //  *** End other variables ***

    //  *** Initialize all elements ***

    public void initHanlders(){// Init hanlders variables
        hMessage = new Handler(){
            @Override
            public void handleMessage(Message msg){
                if(msg.obj != null && !((String)msg.obj).isEmpty()){
                    edtStatus.append("\nSMS: " + (String) msg.obj);
                    notifyShow("Test", "Test");
                }
                this.removeCallbacksAndMessages(null);
            }
        };
        hService = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                String log = (String)msg.obj;
                edtStatus.append(log);
            }
        };
        hSocket = new Handler(){
            @Override
            public void handleMessage(Message msg){
                if(msg.obj == null || ((String) msg.obj).isEmpty()){
                    Toast.makeText(getBaseContext(), "Метод вернул пустую строку", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getBaseContext(), "Результат метода: " + (String) msg.obj, Toast.LENGTH_SHORT).show();
                }
            }
        };
        hSendMessage = new Handler(){
            @Override
            public void handleMessage(Message msg){
                if(msg.obj != null && !((String) msg.obj).isEmpty()){
                    //notifyShow("Text", (String) msg.obj);
                    try {
                        Toast.makeText(activity.getBaseContext(), "Test answers", Toast.LENGTH_SHORT);
                        JSONObject jsonObject = new JSONObject((String) msg.obj);
                        edtStatus.append("\nPhone: +" + jsonObject.getString("phone") + "\nMessage body: " + jsonObject.getString("message"));
                        m.sendMessageWS("+" + jsonObject.getString("phone"), jsonObject.getString("message"), sentPI, null);//deliveredPI
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.i(ConfigClass.LOG_TAG, e.getMessage());
                    }
                }
                this.removeCallbacksAndMessages(null);
            }
        };
    }
    public void initViews(){// Init view elements variables
        edtStatus = (EditText) findViewById(R.id.edt_status);
        btnServiceStart = (Button) findViewById(R.id.btn_service_start);
        btnServiceStop = (Button) findViewById(R.id.btn_service_stop);
    }
    public void initAll(){
        initViews();
        initHanlders();
        serviceIntent = new Intent(this,BackgroundService.class);
        m = new MessageClass(this);
        activity = this;
    }
    //  *** end initialize all elements ***   //
    public void alert(String title, String message){
        MainClass.alert(title, message, this);
    }
    public void notifyShow(String title, String message){
        MainClass.getNotify(title, message, this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initAll();
        WebSocketClass wsc = new WebSocketClass(MainClass.MAIN_URL, this);
        wsc.createEvent(ConfigClass.IO_SMS_SEND, hSendMessage);
        final Handler tHandler = hMessage;
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";
        sentPI = PendingIntent.getBroadcast(getBaseContext(), 0, new Intent(SENT), 0);
        deliveredPI = PendingIntent.getBroadcast(getBaseContext(), 0, new Intent(DELIVERED), 0);
        //registerReceiver(new SentBroadcastReceiver(hMessage, activity), new IntentFilter(SENT));
        //registerReceiver(new DeliveredBroadcastReceiver(hMessage, activity), new IntentFilter(DELIVERED));
    }
    public void startTService(View v){
        startService(new Intent(this, BackgroundService.class));
    }
    public void stopTService(View v){
        stopService(new Intent(this, BackgroundService.class));
    }
}
class SentBroadcastReceiver extends BroadcastReceiver{
    Handler hMessage;
    AppCompatActivity activity;
    SentBroadcastReceiver(Handler handler,AppCompatActivity activity){
        this.hMessage = handler;
        this.activity = activity;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        Message msg = new Message();
        switch(getResultCode()){
            case Activity.RESULT_OK:
                msg.obj = "RESULT_OK";
                Toast.makeText(activity.getBaseContext(), "Show result data: " + this.getResultData() + "\nScheme: " + intent.getScheme(), Toast.LENGTH_SHORT).show();
                System.out.println("\nScheme: " + intent.getDataString());
                hMessage.sendMessage(msg);
                break;
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                Toast.makeText(activity.getBaseContext(), "Show result data: " + this.getResultData(), Toast.LENGTH_SHORT).show();
                msg.obj = "RESULT_ERROR";
                hMessage.sendMessage(msg);
                break;
            case SmsManager.RESULT_ERROR_NO_SERVICE:
                Toast.makeText(activity.getBaseContext(), "Show result data: " + this.getResultData(), Toast.LENGTH_SHORT).show();
                msg.obj = "RESULT_ERROR";
                hMessage.sendMessage(msg);
                break;
            case SmsManager.RESULT_ERROR_NULL_PDU:
                Toast.makeText(activity.getBaseContext(), "Show result data: " + this.getResultData(), Toast.LENGTH_SHORT).show();
                msg.obj = "RESULT_ERROR";
                hMessage.sendMessage(msg);
                break;
            case SmsManager.RESULT_ERROR_RADIO_OFF:
                Toast.makeText(activity.getBaseContext(), "Show result data: " + this.getResultData(), Toast.LENGTH_SHORT).show();
                msg.obj = "RESULT_ERROR";
                hMessage.sendMessage(msg);
                break;
        }
        abortBroadcast();
    }
}
class DeliveredBroadcastReceiver extends BroadcastReceiver {
    Handler hMessage;
    AppCompatActivity activity;
    DeliveredBroadcastReceiver(Handler handler, AppCompatActivity activity) {
        this.hMessage = handler;
        this.activity = activity;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        Message msg = new Message();
        switch(getResultCode()){
            case Activity.RESULT_OK:
                Toast.makeText(activity.getBaseContext(), "Show result data: " + this.getResultData(), Toast.LENGTH_SHORT).show();
                msg.obj = "RESULT_DELIVERED_OK";
                hMessage.sendMessage(msg);
                break;
            case Activity.RESULT_CANCELED:
                Toast.makeText(activity.getBaseContext(), "Show result data: " + this.getResultData(), Toast.LENGTH_SHORT).show();
                msg.obj = "RESULT_CANCELED";
                hMessage.sendMessage(msg);
                break;
        }
        abortBroadcast();
    }
}