package ru.ikaar.android.projects.smsgatewayserviceclient;

/**
 * Created by IKAARuS on 03.03.2016.
 */
public class ConfigClass {
    public static String API_KEY_TITLE = "API_KEY";
    public static String API_KEY = "asdfhlejo;hwnoq-0[bis-p[gj234";
    public static String USER_ID_TITLE = "USER_ID";
    public static String USER_ID = "";
    public static String PORT_MAIN = "9999";
    public static String DOMAIN_MAIN = "http://vm18366.hv8.ru";
    public static String MAIN_URL = DOMAIN_MAIN + ":"  + PORT_MAIN + "/";
    public static String ALERT_FORM_INVALID_TITLE = "Ошибка формы";
    public static String ALERT_FORM_INVALID = "Форма заполнена не полностью.";
    public static String ALERT_AUTH_INVALID_TITLE = "Ошибка авторизации";
    public static String ALERT_AUTH_INVALID = "Приключилась не предвиденная ситуация во время авторизации(Возможно вы указали не верные данные)";
    public static String ALERT_EXCEPTION_TITLE = "Программная ошибка";
    public static String ALERT_EXCEPTION = "Ошибка в работе приложения, если это часто повторяется обратитесь к разработчику.";
    public static String ALERT_QUERY_NULL_TITLE = "Пустой запрос.";
    public static String ALERT_QUERY_NULL = "Запрос не дал результатов.";
    public static String ALERT_QUERY_404 = "{\"error\":\"404\"}";
    public static String TASK_COWORKER_TITLE = "Исполнитель";
    public static String TASK_PRICE_TITLE = "Цена";
    public static String TASK_PRICE_CURRENCY_TITLE = "рублей";
    public static String TASK_CLIENT_NAME_TITLE = "Клиент";
    public static String TASK_ID_TITLE = "Номер задачи";
    public static String IO_SMS_SEND = "SMS_SEND";
    public static String IO__SMS_ANSWER = "SMS_ANSWER";
    public static String IO_SMS_ERROR = "SMS_ERROR";
    public static String IO_SMS_SEND_OK = "SMS_SEND_OK";
    public static String IO_SMS_DELIVERED_OK = "SMS_DELIVERED_OK";
    public static String LOG_TAG = "IKAARuS LOG TAG";
}
