package ru.ikaar.android.projects.smsgatewayserviceclient;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

public class MainClass {
    public static String API_KEY_TITLE = "API_KEY";
    public static String API_KEY = "asdfhlejo;hwnoq-0[bis-p[gj234";
    public static String USER_ID_TITLE = "USER_ID";
    public static String USER_ID = "";
    public static String PORT_MAIN = "9999";
    public static String DOMAIN_MAIN = "http://vm18366.hv8.ru";
    public static String MAIN_URL = DOMAIN_MAIN + ":"  + PORT_MAIN + "/";
    public static String ALERT_FORM_INVALID_TITLE = "Ошибка формы";
    public static String ALERT_FORM_INVALID = "Форма заполнена не полностью.";
    public static String ALERT_AUTH_INVALID_TITLE = "Ошибка авторизации";
    public static String ALERT_AUTH_INVALID = "Приключилась не предвиденная ситуация во время авторизации(Возможно вы указали не верные данные)";
    public static String ALERT_EXCEPTION_TITLE = "Программная ошибка";
    public static String ALERT_EXCEPTION = "Ошибка в работе приложения, если это часто повторяется обратитесь к разработчику.";
    public static String ALERT_QUERY_NULL_TITLE = "Пустой запрос.";
    public static String ALERT_QUERY_NULL = "Запрос не дал результатов.";
    public static String ALERT_QUERY_404 = "{\"error\":\"404\"}";
    public static String TASK_COWORKER_TITLE = "Исполнитель";
    public static String TASK_PRICE_TITLE = "Цена";
    public static String TASK_PRICE_CURRENCY_TITLE = "рублей";
    public static String TASK_CLIENT_NAME_TITLE = "Клиент";
    public static String TASK_ID_TITLE = "Номер задачи";
    public static boolean etxtEmpty(EditText edtxt){
        if(edtxt.getText().toString().isEmpty()){
            return true;
        }else{
            return false;
        }
    }
    public static void getNotify(String title, String message, AppCompatActivity activity){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            Intent intent = new Intent(activity.getBaseContext(), MainActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(activity.getBaseContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(activity.getBaseContext()).
                    setSmallIcon(R.drawable.ic_task).
                    setContentTitle(title).setContentIntent(contentIntent).
                    setContentText(message);
            int notif = 123456;
            Notification notification = builder.build();
            notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
            notification.flags |= Notification.FLAG_AUTO_CANCEL;

            NotificationManager nmp = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
            nmp.notify(notif, notification);
        }
    }
    public static void alert(String title, String message, AppCompatActivity activity)
    {
        new AlertDialog.Builder(activity)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .show();
    }
    public static void pr(String inLine)
    {
        System.out.println(inLine);
    }
}
