package ru.ikaar.android.projects.smsgatewayserviceclient;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.os.Handler;
import android.widget.Toast;

import static ru.ikaar.android.projects.smsgatewayserviceclient.MainClass.pr;

/**
 * Created by IKAARuS on 13.02.2016.
 */
public class BackgroundService extends Service {
    private static final String TAG = "BackgroundService by IKAARuS";
    Handler handler;
    @Override
    public void onCreate() {
        Toast.makeText(getBaseContext(), "Первый запуск", Toast.LENGTH_SHORT).show();
        super.onCreate();
    }

    public void sendLog(String message){
        
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(getBaseContext(), "onStartCommand", Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {

        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public void onDestroy() {
        Toast.makeText(getBaseContext(), "Сервис уничтожен.", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void startDemon(){

    }
}
