package ru.ikaar.android.projects.smsgatewayserviceclient;
    import android.os.Message;
    import org.apache.commons.io.IOUtils;
    import java.io.BufferedInputStream;
    import java.io.IOException;
    import java.io.InputStream;
    import java.net.HttpURLConnection;
    import java.net.URL;
    import android.os.Handler;
public class RRequest implements Runnable{
    public String lastResult = "";
    public URL url;
    public boolean endThread = false;
    public HttpURLConnection mainHC;
    public Handler callbackHandler;
    RRequest(String url, Handler callback) throws IOException {
        this.url = new URL(url);
        callbackHandler = callback;
    }
    @Override
    public void run() {
        Message msg = new Message();
        try {
            mainHC = (HttpURLConnection) this.url.openConnection();
            InputStream in = new BufferedInputStream(mainHC.getInputStream());
            this.lastResult = readStream(in);
            this.endThread = true;
            msg.obj = (String) this.lastResult;
            callbackHandler.sendMessage(msg);
        } catch (IOException e) {
            e.printStackTrace();
            msg.obj = null;
            callbackHandler.sendMessage(msg);
        }
    }
    private String readStream(InputStream in) throws IOException {
        String s = IOUtils.toString(in);
        return s;
    }
}
