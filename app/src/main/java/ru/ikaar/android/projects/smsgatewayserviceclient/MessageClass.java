package ru.ikaar.android.projects.smsgatewayserviceclient;
import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Message;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.os.Handler;
import android.widget.Toast;
public class MessageClass {
    AppCompatActivity activity;
    Handler tHandler;
    MessageClass(AppCompatActivity activity){
        this.activity = activity;
        initPermission(activity);
    }
    public void initPermission(Activity activity){
        if(Build.VERSION.SDK_INT >= 23){
            int smsPermission = activity.checkSelfPermission(Manifest.permission.SEND_SMS);
            if(smsPermission != 0)
                activity.requestPermissions(new String[]{Manifest.permission.SEND_SMS}, 0);
        }
    }
    public void sendMesage(String toPhone, String message, Activity activity, Handler handler){
        SmsManager sms = SmsManager.getDefault();
        PendingIntent pi = PendingIntent.getActivity(
                activity.getBaseContext(),
                0,
                new Intent(activity, Telephony.Sms.class),
                0
        );
        sms.sendTextMessage(toPhone, null, message, pi, null);
        handler.sendMessage(new Message());
    }
    public void tost(String message){
        Toast.makeText(activity.getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }
    public void sendMessageWithStatus(final String toPhone, String message, Handler handler){
        tHandler = handler;
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";
        PendingIntent sentPI;
        sentPI = PendingIntent.getBroadcast(activity.getBaseContext(), 0, new Intent(SENT), 0);
        PendingIntent deliveredPI;
        deliveredPI = PendingIntent.getBroadcast(activity.getBaseContext(), 0, new Intent(DELIVERED), 0);
        BroadcastReceiver br = new BroadcastReceiver(){
            @Override
            public void onReceive(Context ctx, Intent intent){
                Message msg = new Message();
                switch(getResultCode()){

                    case Activity.RESULT_OK:
                        msg.obj = "Phone " + toPhone +  ": " + ConfigClass.IO_SMS_SEND_OK;
                        tost("Phone " + toPhone +  ":" + (String)msg.obj);
                        tHandler.sendMessage(msg);
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        msg.obj = "Phone " + toPhone +  ": " + ConfigClass.IO_SMS_ERROR;
                        tHandler.sendMessage(msg);
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        msg.obj = "Phone " + toPhone +  ": " + ConfigClass.IO_SMS_ERROR;
                        tHandler.sendMessage(msg);
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        msg.obj = "Phone " + toPhone +  ": " + ConfigClass.IO_SMS_ERROR;
                        tHandler.sendMessage(msg);
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        msg.obj = "Phone " + toPhone +  ": " + ConfigClass.IO_SMS_ERROR;
                        tHandler.sendMessage(msg);
                        break;
                }
            }
        };
        BroadcastReceiver brD = new BroadcastReceiver(){
            @Override
            public void onReceive(Context ctx, Intent intent){
                Message ms = new Message();
                switch(getResultCode()){
                    case Activity.RESULT_OK:
                        ms.obj = "Phone " + toPhone +  ": " + ConfigClass.IO_SMS_DELIVERED_OK;
                        tHandler.sendMessage(ms);
                        tost((String)ms.obj);
                        break;
                    case Activity.RESULT_CANCELED:
                        ms.obj = "Phone " + toPhone +  ": " + ConfigClass.IO_SMS_ERROR;
                        tHandler.sendMessage(ms);
                        break;
                }
            }
        };
        activity.registerReceiver(br, new IntentFilter(SENT));
        activity.registerReceiver(brD, new IntentFilter(DELIVERED));
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(toPhone, null, message, sentPI, deliveredPI);
    }
    public void sendMessageWS(final String toPhone, String message, PendingIntent sentPI, PendingIntent deliveredPI){
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(toPhone, null, message, sentPI, deliveredPI);
    }

}
